const express = require("express")
const bodyParser = require("body-parser")

const app = express()
const PORT = process.env.PORT || 56201

app.use(express.json())

const router = express.Router()

app.use(router)

const requirePlaintext = (req, res, next) => {
    if (req.headers["content-type"] !== "text/plain") {
        res.status(400).send("Endpoint requires text/plain")
    } else {
        next()
    }
}

router.post("/square", requirePlaintext, bodyParser.text({ type: "text/plain" }), (req, res) => {
    const input = Number.parseFloat(req.body)
    if (isNaN(input)) {
        res.status(400).send("The body should be a number")
        return
    }

    const resBody = {
        number: input,
        square: input * input
    }
    res.status(200).json(resBody)
})

router.post("/reverse", requirePlaintext, bodyParser.text({ type: "text/plain" }), (req, res) => {
    const reversed = req.body.split("").reverse().join("")
    res.status(200).send(reversed)
})

router.get("/date/:year/:month/:day", (req, res) => {
    const { year, month, day } = req.params

    if (!/^\d{4}$/.test(year) || !/^\d{1,2}$/.test(month) || !/^\d{1,2}$/.test(day)) {
        return res.status(400).send("Year, month, and day must be numeric. Year must be 4 digits, month and day must be 1 or 2 digits.")
    }

    const date = new Date(year, month - 1, day)
    if (date.getFullYear() !== parseInt(year, 10) || date.getMonth() + 1 !== parseInt(month, 10) || date.getDate() !== parseInt(day, 10)) {
        return res.status(400).json({ error: "Invalid date." })
    }

    const today = new Date()
    today.setHours(0, 0, 0, 0)

    const diffTime = date.getTime() - today.getTime()
    const diffDays = Math.abs(Math.round(diffTime / (1000 * 60 * 60 * 24)))

    const response = {
        weekDay: date.toLocaleDateString("en-US", { weekday: "long" }),
        isLeapYear: new Date(year, 1, 29).getMonth() === 1,
        difference: diffDays
    }

    res.json(response)
})

app.listen(PORT, ()=>{
    console.log(`Server is running on port ${PORT}`)
})
